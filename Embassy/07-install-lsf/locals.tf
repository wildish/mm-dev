data "terraform_remote_state" "state" {
  backend = "local"
  config = {
    path = "../00-config/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "../01-vpc/terraform.tfstate"
  }
}

data "terraform_remote_state" "bastion" {
  backend = "local"
  config = {
    path = "../03-bastion/terraform.tfstate"
  }
}

data "terraform_remote_state" "hosts" {
  backend = "local"
  config = {
    path = "../04-hosts/terraform.tfstate"
  }
}

locals {
  state = data.terraform_remote_state.state.outputs.state

  namespace = data.terraform_remote_state.state.outputs.namespace
  user = data.terraform_remote_state.state.outputs.tenancy_user
  lsf_source = data.terraform_remote_state.state.outputs.lsf_source
  lsf_version = data.terraform_remote_state.state.outputs.lsf_version
  lsf_host_range = data.terraform_remote_state.state.outputs.lsf_host_range

  public_key = data.terraform_remote_state.state.outputs.public_key
  private_key = data.terraform_remote_state.state.outputs.private_key
  key_pair = data.terraform_remote_state.vpc.outputs.key_pair

  bastion_ip  = data.terraform_remote_state.bastion.outputs.bastion_ip
  floating_ip = data.terraform_remote_state.vpc.outputs.floating_ip

  host_count = data.terraform_remote_state.state.outputs.host_count
  host_ips = data.terraform_remote_state.hosts.outputs.host_ips
}
