data "terraform_remote_state" "state" {
  backend = "local"
  config = {
    path = "../00-config/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "../01-vpc/terraform.tfstate"
  }
}

locals {
  namespace = "${data.terraform_remote_state.state.outputs.namespace}"

  user = "${data.terraform_remote_state.state.outputs.tenancy_user}"
  floating_ip = "${data.terraform_remote_state.vpc.outputs.floating_ip}"

  public_key = "${data.terraform_remote_state.state.outputs.public_key}"
  private_key = "${data.terraform_remote_state.state.outputs.private_key}"

  bastion_volumes = "${data.terraform_remote_state.state.outputs.bastion_volumes}"
}
