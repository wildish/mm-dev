#!/bin/bash

file=$1
if [ "$file" == "" ]; then
  echo "Need a json file with group information"
  exit 1
fi

length=`cat $file | jq length`
if [ "$length" == "" ]; then
  echo "No group information, exiting..."
  exit 0
fi

index=0
while [ $index -lt $length ]
do
  echo "jq: `cat $file | jq .[${index}]`"
  group=`cat $file | jq .[${index}].name | tr -d '"'`
  id=`   cat $file | jq .[${index}].id   | tr -d '"'`

  if [ "$id" != "null" ]; then
    opt_id="--gid $id"
  else
    opt_id=""
  fi

  echo "Install $group($id) on host `hostname -s`"
  groupadd --force $group $opt_id

  index=`expr 1 + $index`
done

echo "All done..."
