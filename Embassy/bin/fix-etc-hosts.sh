#!/bin/bash

namespace=$1
if [ "$namespace" == "" ]; then
  echo "Need a namespace"
  exit 1
fi

ip=$2
if [ "$ip" == "" ]; then
  echo "Need an IP address"
  exit 1
fi

short=`hostname --short`
shorter=`echo $short | sed -e "s%^${namespace}-%%"`

(
  echo "127.0.0.1 localhost localhost.localdomain localhost4 localhost4.localdomain4"
  echo "::1       localhost localhost.localdomain localhost6 localhost6.localdomain6"
  echo " "
  echo "$ip $short $shorter"
) | tee /etc/hosts
