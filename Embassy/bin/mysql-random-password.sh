#!/bin/bash

#
# MySQL 8.0 has hard requirements for passwords, this function
# attempts to generate passwords that conform, but to weakened rules:
# See mysql-server.sh for those rule changes
#

elements=()

len1=`expr 7 + $RANDOM % 9`
elements[0]=`cat /dev/urandom | tr -dc [:digit:] | head -c $len1`

len2=`expr 30 - $len1`
str=`cat /dev/urandom | tr -dc A-Za-z | head -c $len2`
c1=${str:0:1} ; c1=${c1,} # Bash v4, translate to lowercase
c2=${str:1:1} ; c2=${c2^} # Bash v4, translate to uppercase
elements[1]=${c1}${c2}${str:2:$len2}

elements=( $(echo ${elements[@]} | shuf -) )
printf "%s" "${elements[@]}"
