#!/bin/sh

yum -y install epel-release.noarch
yum group install -y 'Development Tools'
yum install -y kernel-devel nfs-utils bind-utils mailx ansible git wget \
  python-pip python-devel anaconda jq sysstat pdsh screen tmux \
  pigz pbzip2 lbzip2 perf

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

yum install -y openldap openldap-clients openldap-servers openldap-clients nss-pam-ldapd \
  dnsmasq \
  docker-ce \
  singularity \
  libarchive-devel squashfs-tools \
  ed.x86_64 mailx net-tools java-1.8.0-openjdk \
  mysql-community-server \
  python2-pip gcc python-devel mysql-devel \
  munge{,-devel} \
  readline-devel pam-devel perl-ExtUtils-MakeMaker

yum install -y https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
yum install -y mysql-community-server

groupadd docker
usermod -aG docker root
chmod 666 /var/run/docker.sock
systemctl enable docker
systemctl start docker

yum update -y
