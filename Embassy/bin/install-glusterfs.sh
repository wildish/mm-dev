# from https://wiki.centos.org/HowTos/GlusterFSonCentOS

yum install -y centos-release-gluster41
yum install -y glusterfs glusterfs-cli glusterfs-libs glusterfs-server

umount /local
pvcreate --force /dev/vdb
vgcreate vg_gluster /dev/vdb

lvcreate -L 4999G -n brick1 vg_gluster
mkfs.xfs /dev/vg_gluster/brick1
mkdir -p /bricks/brick1

# Now, and after reboot, mount the bricks
mount /dev/vg_gluster/brick1 /bricks/brick1
# (in /etc/fstab: /dev/vg_gluster/brick1 /bricks/brick1 xfs defaults 0 0)

firewall-cmd --zone=public --add-port=24007-24008/tcp --permanent
firewall-cmd --reload
systemctl enable glusterd.service
systemctl start glusterd.service

# From one host:
bastion> for i in h-000{0..3}; do gluster peer probe $h; done

gluster volume create glustervol1 replica 2 transport tcp h-000{0,1,2,3}:/bricks/brick1/brick
gluster volume start glustervol1

# on all hosts
firewall-cmd --zone=public --add-service=nfs --add-service=samba --add-service=samba-client --permanent
firewall-cmd --zone=public --add-port=111/tcp --add-port=139/tcp --add-port=445/tcp --add-port=965/tcp --add-port=2049/tcp \
--add-port=38465-38469/tcp --add-port=631/tcp --add-port=111/udp --add-port=963/udp --add-port=49152-49251/tcp  --permanent
firewall-cmd --reload

# set 'Defaultvers=3' in /etc/nfsmount.conf under the NFSMount_Global_Options
# disable firewall
# Don't use NFS...
# mount -t nfs bastion:/glustervol1 /mnt

#
# ...use glusterfs!

# add to /etc/fstab:
# bastion:/glustervol1       /mnt  glusterfs   defaults  0  0
mount -t glusterfs bastion:/glustervol1 /mnt

# Check that hosts resolve correctly, /etc/resolv.conf is re-written on reboot
