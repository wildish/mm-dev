#!/bin/bash

state=$1
file=$2
if [ "$file" == "" ]; then
  echo "Need the state directory and a json file with user information"
  exit 1
fi

length=`cat $file | jq length`
if [ "$length" == "" ]; then
  echo "No user information, exiting..."
  exit 0
fi

key_dir=${state}/user-keys
[ -d $key_dir ] || mkdir -p $key_dir

index=0
while [ $index -lt $length ]
do
  user=`   cat $file | jq .[${index}].user     | tr -d '"'`
  key=`    cat $file | jq .[${index}].key      | tr -d '"'`
  nologin=`cat $file | jq .[${index}].nologin  | tr -d '"'`

  index=`expr 1 + $index`

  if [ ! "$nologin" == "null" ]; then
    # user not allowed to log in, so no SSH key needed
    continue
  fi

  key_file=${key_dir}/key.$user
  if [ -f $key_file ]; then
    echo "$key_file already exists, won't clobber"
  else
    ssh-keygen -t rsa -b 4096 -C "Auto-generated key" -P "" -f $key_file
    if [ "$key" != "null" ]; then
      key_file="${key_file}.pub.provided"
      [ -f $key_file ] && rm -f $key_file
      echo $key | tee $key_file >/dev/null
    fi
  fi
done

echo "All done..."
