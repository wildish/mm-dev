#!/bin/bash

dest=/data/$USER/test/
mkdir -p $dest

(
  echo "#!/bin/sh"
  echo "#BSUB -o $dest/test.%J.out"
  echo "#BSUB -e $dest/test.%J.err"
  echo " "
  echo "date"
  echo "hostname"
  echo "sleep 20"
  echo "echo 'All done'"
) | tee test.sh
chmod +x test.sh

for i in {0..10}
do
  bsub < test.sh
done

bjobs

