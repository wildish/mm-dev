#!/bin/bash

file=$1
if [ "$file" == "" ]; then
  echo "Need a json file with mount point information"
  exit 1
fi

length=`cat $file | jq length`
if [ "$length" == "" ]; then
  echo "No mount-point information, exiting..."
  exit 0
fi

index=0
while [ $index -lt $length ]
do
  echo "Processing entry $index"
  export=`cat $file | jq .[${index}].export | tr -d '"'`
  label=` cat $file | jq .[${index}].label  | tr -d '"'`
  id=`    cat $file | jq .[${index}].id     | tr -d '"'`
  mount=` cat $file | jq .[${index}].mount  | tr -d '"'`
  echo "Export=$export, Label=$label, Id=$id, Mount=$mount" 
  [ -d $mount ] || mkdir -p $mount
  if [ "$export" == "rw" ]; then
    options="defaults,rw"
  else
    options="defaults,ro"
  fi
  echo "bastion:$mount $mount nfs ${options} 0 0" | tee -a /etc/fstab

  index=`expr 1 + $index`
done

mount -a -t nfs
echo "All done..."
