#!/bin/bash

slurmconf=/etc/slurm/slurm.conf
if [ ! -f $slurmconf ]; then
  echo "No config file, spitting the dummy!"
  exit 1
fi

is_bastion=`hostname -s | grep -c bastion`
if [ $is_bastion -eq 1 ]; then
  systemctl start slurmdbd
  sleep 3
  systemctl start slurmctld
fi
systemctl start slurmd
