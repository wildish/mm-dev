#!/bin/bash

cd `dirname $0`
here=`pwd`
vsn=`cat slurm-version.env`

is_bastion=`hostname -s | grep -c bastion`

cd /slurm
munge_key_src="munge.key"
munge_key_dst="/etc/munge/munge.key"
if [ -f $munge_key_dst ]; then
  echo "Munge key ($munge_key_dst) already exists, assuming munge installed"
else
  if [ ! -f $munge_key_src ]; then
    if [ $is_bastion -eq 1 ]; then
      dd if=/dev/urandom bs=1 count=1024 >$munge_key_src
    else
      echo "No munge key (`pwd`/$munge_key_src) and not on bastion, spitting the dummy"
      exit 1
    fi
  fi
  cp $munge_key_src $munge_key_dst
  chown munge.munge $munge_key_src $munge_key_dst
  chmod 400 $munge_key_src $munge_key_dst

  systemctl enable munge
  systemctl start munge
fi

cd /slurm/slurm-${vsn}
make install
  
dirs="/var/log/slurm /var/spool/slurm /var/spool/slurmd /var/run/slurm /etc/slurm"
mkdir -p $dirs
cp etc/cgroup.conf.example /etc/slurm/cgroup.conf
chown -Rf slurm.slurm $dirs

servicedir=/etc/systemd/system
if [ $is_bastion -eq 1 ]; then
  cat etc/slurmctld.service | \
    sed -e 's%/var/run/%/var/run/slurm/%' | \
    tee $servicedir/slurmctld.service
  cat etc/slurmdbd.service | \
    sed -e 's%/var/run/%/var/run/slurm/%' | \
    tee $servicedir/slurmdbd.service
fi
cat etc/slurmd.service | \
  sed -e 's%/var/run/%/var/run/slurm/%' | \
  tee $servicedir/slurmd.service
chmod 644 $servicedir/slurm*.service

echo "export LD_LIBRARY_PATH=/usr/lib/slurm" | tee /etc/profile.d/slurm.sh
slurmd -C | grep NodeName | tee /slurm/hosts/`hostname -s`

systemctl enable slurmd
# systemctl start  slurmd

# scontrol update nodename=`hostname` state=IDLE

# set up logrotate
if [ $is_bastion ]; then
  cp $here/slurm.logrotate /etc/logrotate.d/slurm
else
  cat $here/slurm.logrotate | egrep -v 'slurmctld|slurmdbd' | tee /etc/logrotate.d/slurm >/dev/null
fi