#!/bin/bash

random_password () {
#
# MySQL 8.0 has hard requirements for passwords, this function
# attempts to generate passwords that conform...
  elements=()

  # len1=`expr 1 + $RANDOM % 9`
  # elements[0]=`cat /dev/urandom | tr -dc  '!*|@#$%^-_=+,<>/?' | head -c $len1`

  # len2=`expr 1 + $RANDOM % 9`
  # elements[1]=`cat /dev/urandom | tr -dc [:digit:] | head -c $len2`

  # len3=`expr 30 - $len1 - $len2`
  # str=`cat /dev/urandom | tr -dc A-Za-z | head -c $len3`
  # c1=${str:0:1} ; c1=${c1,}
  # c2=${str:1:1} ; c2=${c2^}
  # elements[2]=${c1}${c2}${str:2:$len3}

  len1=`expr 7 + $RANDOM % 9`
  elements[0]=`cat /dev/urandom | tr -dc [:digit:] | head -c $len1`

  len2=`expr 30 - $len1`
  str=`cat /dev/urandom | tr -dc A-Za-z | head -c $len2`
  c1=${str:0:1} ; c1=${c1,}
  c2=${str:1:1} ; c2=${c2^}
  elements[1]=${c1}${c2}${str:2:$len2}

  elements=( $(echo ${elements[@]} | shuf -) )
  printf "%s" "${elements[@]}"
}

users="$1"

yum install -y https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
yum install -y mysql-community-server

#
# Relax the password constraints, but not too far
(
    echo "validate_password.length = 20"
    echo "validate_password.mixed_case_count = 1"
    echo "validate_password.number_count = 1"
    echo "validate_password.special_char_count = 0"
) | tee -a /etc/my.cnf

systemctl restart mysqld.service

pwfile="/root/mysql-password.txt"
if [ -f $pwfile ]; then
  echo "$pwfile already exists"
  pw=`cat $pwfile`
else
  echo "Generating password"
  pw=$(random_password)
  echo "$pw" | tee $pwfile >/dev/null
  chmod 400 $pwfile
#
# Now we need to set that password for the root user, and this can get tricky...
  init_pw=`cat /var/log/mysqld.log | grep 'A temporary password' | awk '{ print $NF }'`
  init_file="/root/init-mysql-pw.sql"
  (
    echo "ALTER USER 'root'@'localhost' IDENTIFIED BY '$pw';"
    echo "SHOW VARIABLES LIKE 'validate_password%';"
  ) | tee $init_file
  mysql --user root --password=$init_pw --connect-expired-password < $init_file
  /bin/rm -f $init_file
fi

script="/root/mysql.sql"

(
  echo "DELETE FROM mysql.user WHERE User='';"
  echo "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
  echo "DROP DATABASE IF EXISTS test;"
  echo "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';"

  if [ -f "$users" ]; then
    for user in `cat $users | jq '.[].user' | tr -d '"'`
    do
      echo " "
      user_pw=$(random_password)
      homedir=$( getent passwd $user | cut -d: -f6 )
      user_pwfile=$homedir/mysql-password.txt
      echo $user_pw | tee $user_pwfile >/dev/null
      chown $user.$user $user_pwfile
      chmod 400 $user_pwfile
      echo "CREATE USER IF NOT EXISTS '${user}'@'localhost' IDENTIFIED BY '${user_pw}';"
      echo "CREATE USER IF NOT EXISTS '${user}'@'%' IDENTIFIED BY '${user_pw}';"
      echo "CREATE DATABASE IF NOT EXISTS ${user}_db;"
      echo "GRANT ALL PRIVILEGES ON ${user}_db.* TO '${user}'@'localhost';"
      echo "GRANT ALL PRIVILEGES ON ${user}_db.* TO '${user}'@'%';"
    done
  fi
) | tee $script

set -ex
firewall-cmd --zone=public --add-service=mysql --permanent
firewall-cmd --reload
systemctl restart mysqld
mysql --user=root --password=$pw < $script
/bin/rm -f $script

echo "MySQL root password is in $pwfile"
