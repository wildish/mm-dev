#!/bin/bash

cd /data

dir=aws
if [ ! -d $dir ]; then
  file=awscliv2.zip
  if [ ! -f $file ]; then
    curl https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o $file
    unzip $file
  fi
fi

sudo ./aws/install

if [ -f $file ]; then
  /bin/rm -f $file
fi
