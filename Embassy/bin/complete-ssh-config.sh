#!/bin/bash
#
# This script finalises ssh config file for ssh'ing into the bastion and hosts.
#
state=$1
namespace=$2
if [ "$namespace" == "" ]; then
  echo "Need the state directory and namespace"
  exit 1
fi

cd $state
state=`pwd`
config=$state/ssh-config
[ -f $config ] && /bin/rm -f $config
bastion_ip=`cat ssh-config.bastion | grep HostName | awk '{ print $2 }'`

echo "Config=$config, bastion IP=$bastion_ip"

cp ${config}.bastion $config

cd fragments/ip
for f in `find . -type f | sed -e 's%^\.\/%%'`
do
  host=`cat $f | sed -e "s%^${namespace}-%%"`
  ip=`echo $f | tr '/' '.'`
  if [ $host != "bastion" ]; then
    echo
    (
      echo " "
      echo "Host $host"
      echo " ProxyCommand ssh -F \"$config\" bastion -W %h:%p"
      echo "  HostName $ip"
    ) | tee -a $state/ssh-config
  fi
done
chmod 600 $config >/dev/null
