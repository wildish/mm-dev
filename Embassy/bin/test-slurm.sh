#!/bin/bash

dest="/data/$USER/slurm/test"
if [ ! -d $dest ]; then
  mkdir -p $dest
fi

(
  echo "#!/bin/bash"
  echo "#SBATCH --output $dest/test-slurm-batch-%J.out"
  echo "#SBATCH --error  $dest/test-slurm-batch-%J.err"
  echo "#SBATCH --ntasks 1"
  echo "#SBATCH --mem 1G"
  echo " "
  echo "hostname"
  echo "sleep 60"
) | tee test-slurm-batch.sh

for i in `seq 0 999`
do
  sbatch < test-slurm-batch.sh
done

sleep 2
squeue

