#!/bin/bash

(
  node=$1
  if [ "$node" == "" ]; then
    echo "Need a node-name to reboot"
    exit 1
  fi

  namespace=$2
  if [ "$namespace" != "" ]; then
    echo "Namespace is $namespace"
    node=`echo $node | sed -e "s%^${namespace}-%%"`
    echo "So now, node is $node"
  fi

  config="../state/ssh-config"
  if [ -f "$config.$node" ]; then
    config="$config.$node"
  fi
  ssh="ssh -F $config"

  echo "Rebooting $node"
  cd `dirname $0`
  set -x
  $ssh $node uname -r
  # $ssh $node ls /boot
  $ssh $node sudo shutdown -r now

  while true
  do
    sleep 5
    $ssh $node uptime
    if [ $? -eq 0 ]; then
      echo "$node is back up!"
      $ssh $node sudo yum update -y
      $ssh $node uname -r
      exit 0
    fi
  done

) 2>&1 | tee reboot.log