#!/bin/bash

rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
rpm -Uvh https://www.elrepo.org/elrepo-release-7.0-4.el7.elrepo.noarch.rpm
yum-config-manager --enable elrepo-kernel
yum install -y kernel-ml

# grub_cfg="/boot/efi/EFI/centos/grub.cfg"
grub_cfg="/boot/grub2/grub.cfg"
awk -F\' '$1=="menuentry " {print i++ " : " $2}' $grub_cfg
choice=`awk -F\' '$1=="menuentry " {print i++ " : " $2}' $grub_cfg | grep elrepo | grep -v Rescue | awk '{ print $1 }'`

grub2-set-default $choice # or whatever value you get from the above

grub2-mkconfig -o $grub_cfg
