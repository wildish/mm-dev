#!/bin/bash

bastion_ip=$1

hosts=$2
if [ "$hosts" != "" ]; then
  if [ -f $hosts ]; then
    cat $hosts | sudo tee -a /etc/hosts
    /bin/rm -f $hosts
  fi
fi
mkdir -p /etc/dhcp

if [ -f bin/dhclient-exit-hooks ]; then
  cat bin/dhclient-exit-hooks | \
    sed -e "s%_INSERT_BASTION_IP_%$bastion_ip%" | \
    tee /etc/dhcp/dhclient-exit-hooks
  chmod +x /etc/dhcp/dhclient-exit-hooks
  /etc/dhcp/dhclient-exit-hooks
  /bin/rm -f bin/dhclient-exit-hooks
fi

systemctl enable dnsmasq
systemctl restart dnsmasq

if [ "`which firewall-cmd`" !=  "" ]; then
  firewall-cmd --permanent --zone=public --add-service=dns
  firewall-cmd --reload
fi
