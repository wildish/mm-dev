#!/bin/bash

cd `dirname $0`
vsn=`cat slurm-version.env`
export PATH=${PATH}:`pwd`

[ -d /slurm ] || mkdir /slurm
cd /slurm

#
# Install & configure Munge
# TW /etc/munge/munge.key this to be distributed to all nodes!
# munge_key_src="munge.key"
# munge_key_dst="/etc/munge/munge.key"
# if [ -f $munge_key_dst ]; then
#   echo "Munge key ($munge_key_dst) already exists, assuming munge installed"
# else
#   echo "Install munge"
#   yum install -y munge{,-devel}
#   if [ ! -f $munge_key_src ]; then
#     dd if=/dev/urandom bs=1 count=1024 >$munge_key_src
#   fi
#   cp $munge_key_src $munge_key_dst
#   chown munge.munge $munge_key_src $munge_key_dst
#   chmod 400 $munge_key_src $munge_key_dst

#   systemctl enable munge
#   systemctl start munge
# fi

here=`pwd`

#
# Download, build and install Slurm
dir=slurm-${vsn}
[ -d $dir ] && rm -rf $dir
file=slurm-${vsn}.tar.bz2
url=https://download.schedmd.com/slurm/${file}
if [ ! -f $file ]; then
  set -ex
  wget --no-check-certificate -O $file $url
  yum install -y perl-ExtUtils-MakeMaker libcurl-devel hdf5{,-devel} json-parser lz4{,-devel}
  yum install -y openssl openssl-devel pam-devel numactl numactl-devel hwloc hwloc-devel \
                 lua lua-devel readline-devel rrdtool-devel ncurses-devel man2html libibmad \
                 libibumad munge munge-libs munge-devel rng-tools python3
  set +ex
fi

if [ -d "/etc/slurm" ]; then
  echo "Slurm config directory /etc/slurm already exists"
else
  echo "Build and do basic configuration for slurm"
  # adduser --no-create-home --user-group --home-dir /home/user/slurm --shell /bin/false slurm
  tar xf $file
  cd $dir
  
  ./configure --prefix=/usr --sysconfdir=/etc/slurm --enable-debug --with-hdf5 --with-hwloc
  make -j 4 # && make install
  
  dirs="/var/log/slurm /var/spool/slurm /var/spool/slurmd /var/run/slurm /etc/slurm"
  mkdir -p $dirs
  cp etc/cgroup.conf.example /etc/slurm/cgroup.conf
  chown -Rf slurm.slurm $dirs

  # servicedir=/etc/systemd/system
  # cat etc/slurmctld.service | \
  #   sed -e 's%/var/run/%/var/run/slurm/%' | \
  #   tee $servicedir/slurmctld.service
  # cat etc/slurmdbd.service | \
  #   sed -e 's%/var/run/%/var/run/slurm/%' | \
  #   tee $servicedir/slurmdbd.service
  # cat etc/slurmd.service | \
  #   sed -e 's%/var/run/%/var/run/slurm/%' | \
  #   tee $servicedir/slurmd.service
  # chmod 644 $servicedir/slurm*.service
  
  echo "export LD_LIBRARY_PATH=/usr/lib/slurm" | tee /etc/profile.d/slurm.sh
  firewall-cmd --permanent --zone=public --add-port=6817/tcp
  firewall-cmd --permanent --zone=public --add-port=6818/tcp
  firewall-cmd --permanent --zone=public --add-port=7321/tcp
  firewall-cmd --reload
fi

#
# SlurmDBD
#
cd $here
slurm_pw_file="/etc/slurm/slurmdbd.mysql.txt"
if [ -f $slurm_pw_file ]; then
  echo "Slurm DB password file ($slurm_pw_file) already exists, assuming slurmdbd configured"
else
  echo "Configure slurmdbd"
  slurm_pw=`mysql-random-password.sh`
  echo $slurm_pw | tee $slurm_pw_file >/dev/null
  chown slurm.slurm $slurm_pw_file
  chmod 400 $slurm_pw_file

  (
    echo "AuthType=auth/munge"
    echo "AuthInfo=/var/run/munge/munge.socket.2"
    echo "DbdHost=localhost"
    echo "#DbdBackupHost=localhost"
    echo "StorageHost=localhost"
    echo "StorageLoc=slurm_acct_db"
    echo "StoragePass=$slurm_pw"
    echo "StorageType=accounting_storage/mysql"
    echo "StorageUser=slurm"
    echo "LogFile=/var/log/slurm/slurmdbd.log"
    echo "PidFile=/var/run/slurm/slurmdbd.pid"
    echo "SlurmUser=slurm"
  ) | tee /etc/slurm/slurmdbd.conf
  chown slurm.slurm /etc/slurm/slurmdbd.conf

#
# ensure slurm run directory is created on boot
  echo 'd /var/run/slurm 0755 slurm slurm - -' | tee /etc/tmpfiles.d/slurm.conf

  pwfile=/root/mysql-password.txt
  if [ ! -f $pwfile ]; then
    echo "No root MySQL password found (no $pwfile), spitting the dummy"
    exit 1
  fi
  pw=`cat $pwfile`


  slurmdbd_sql="/root/slurmdbd.sql"
  (
    echo "CREATE DATABASE IF NOT EXISTS slurm_acct_db;"
    echo "USE slurm_acct_db;"
    echo "SHOW VARIABLES LIKE 'validate_password%';"
    echo "CREATE USER IF NOT EXISTS 'slurm'@'localhost' IDENTIFIED BY '$slurm_pw';"
    # echo "CREATE USER 'slurm'@'localhost';"
    # echo "ALTER USER 'slurm'@'localhost' IDENTIFIED BY '$slurm_pw';"
    echo "GRANT ALL ON slurm_acct_db.* TO 'slurm'@'localhost' WITH GRANT OPTION;"
  ) | tee $slurmdbd_sql
  mysql -u root --password=$pw < $slurmdbd_sql
  rm -f $slurmdbd_sql
fi

mkdir -p /slurm/hosts
slurmd -C | grep NodeName | tee /slurm/hosts/`hostname -s`
