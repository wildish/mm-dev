#!/bin/bash

scontrol reconfigure

systemctl stop  slurmd
systemctl stop  slurmctld
systemctl start slurmctld
systemctl start slurmd

sinfo -N -o "%N %m %c %T"

for node in `sinfo --Node --noheader --format "%N" --states "down,drain"`
do
  scontrol update NodeName=$node State=Resume
done

scontrol show node | egrep -i 'nodename|reason'

