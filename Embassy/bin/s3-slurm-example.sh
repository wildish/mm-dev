#!/bin/bash
#SBATCH --output /data/test-slurm-s3-%J.out
#SBATCH --error  /data/test-slurm-s3-%J.err
#SBATCH --ntasks 1
#SBATCH --mem 1G

#
# You can copy a single file to/from the object store:
echo "Hello Object World" | tee helloworld.txt
$S3 cp helloworld.txt $S3_BUCKET
$S3 ls $S3_BUCKET
$S3 cp $S3_BUCKET/helloworld.txt new-helloworld.txt
$S3 rm $S3_BUCKET/helloworld.txt

#
# You can organise your files hierarchically, but there are no directories as such:
$S3 cp helloworld.txt $S3_BUCKET/a/b/c/file.txt # N.B. No need to create 'directories'
$S3 ls --recursive $S3_BUCKET
$S3 rm $S3_BUCKET/a/b/c/file.txt
$S3 ls --recursive $S3_BUCKET

# $S3 ls help

echo "See also http://docs.embassy.ebi.ac.uk/userguide/User_Guide.html#s3-object-store"
