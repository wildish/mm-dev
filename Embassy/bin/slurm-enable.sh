#!/bin/bash

cd `dirname $0`
here=`pwd`
vsn=`cat slurm-version.env`
cd /slurm/slurm-${vsn}

is_bastion=`hostname -s | grep -c bastion`

shost=`hostname --short`
slurmconf=/etc/slurm/slurm.conf
if [ ! -f $slurmconf ]; then
  if [ $is_bastion -eq 1 ]; then
    namespace=$1
    host_count=$2
    if [ "$host_count" == "" ]; then
      echo "Need namespace and host_count arguments"
      exit 1
    fi
    host_count=`expr $host_count - 1`
    workers=`printf "%s-h-[0000-%04i]" $namespace $host_count`
    (
      cat $here/slurm.conf.template | \
        sed -e "s%BASTION%$shost%g" \
            -e "s%WORKERS%$workers%g"
      cat /slurm/hosts/*
    ) | tee slurm.conf
  fi
  if [ ! -f slurm.conf ]; then
    echo "No slurm.conf in `pwd`: spitting the dummy"
    exit 1
  fi
  cp slurm.conf $slurmconf
  chown slurm.slurm $slurmconf
fi

if [ $is_bastion -eq 1 ]; then
  systemctl enable slurmdbd
  systemctl enable slurmctld
fi
systemctl enable slurmd

firewall-cmd --zone=public --add-port=6817/tcp --add-port=6818/tcp --permanent
firewall-cmd --reload

# sacctmgr add cluster cluster
# scontrol update nodename=`hostname` state=IDLE
