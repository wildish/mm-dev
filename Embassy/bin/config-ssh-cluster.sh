#!/bin/bash

key=$1
user=$2

echo "ssh-config-cluster got $1 $2"

if [ "$user" == "" ]; then
  echo "Need the key file and the user nae"
  exit 1
fi

config="/home/$user/.ssh/config"

(
    echo "Host *"
    echo "  User $user"
    echo "  StrictHostKeyChecking no"
    echo "  IdentityFile $key"
    echo "  UserKnownHostsFile /dev/null"
    echo " "
) | tee $config

ls -l /home/$user/.ssh
chmod 600 $config $key