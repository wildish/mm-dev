resource "null_resource" "create_state" {
  provisioner "local-exec" {
    command = <<EOF
      rm -rf ${var.state} && mkdir -p ${var.state}

      echo '${jsonencode( var.users )}' | tee ${var.state}/metadata.users.json >/dev/null
      echo '${jsonencode( var.groups )}' | tee ${var.state}/metadata.groups.json >/dev/null
      echo '${jsonencode( var.bastion_volumes )}' | tee ${var.state}/metadata.bastion_volumes.json >/dev/null

      ../bin/user-keygen.sh ${var.state} ${var.state}/metadata.users.json
EOF
  }
}
