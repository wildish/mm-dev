namespace = "mm-dev"

external_network = "ext-net"
tenancy_user = "centos"

bastion_image = "bootstrap" # "centos7"
bastion_disk_gb = 20
bastion_flavour = "s1.large"

bastion_volumes = [
  # {
  #   mount = "/slurm",
  #   size = 5,
  #   label = "SLURM",
  #   export = "rw"
  # },
  {
    mount = "/lsf",
    size = 10,
    label = "LSF",
    export = "rw"
  },
  {
    mount = "/home/user",
    size = 40,
    label = "USER",
    export = "rw"
  },
  {
    mount = "/data",
    size = 1490,
    label = "DATA",
    export = "rw",
    mode = "3777",
    group = "mm"
  },
]

host_image = "bootstrap"
host_disk_gb = 20
host_flavour = "s1.gargantuan"
host_count = 20

groups = [
  { name = "mm" }
]

users = [
  {
    user    = "slurm",
    id      = "9999",
    sudo    = "no",
    nologin = "yes",

  },
  {
    user = "maxim",
    id   = "2877",
    sudo = "no",
    home = "/home/user/maxim",
    groups = [ "mm" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9qMALafVAJc5m3+lA3Qva0JdmgzZI+JXaJkeCwZSnQ+VIybkGFehSIGYsF4pggfMQcRVaNhfCoo5xa8oEubcu6rhUMaeT4CxK1sdXLlZSjV7aAJ2Zc6NMPWZzUcbXId+dfLPtmEo+SaB6dbcQHlGWg1Tkx2Sy9a5mZKsWoQTAw10kQW4lsv91xF+5dTP7QVxVWtb1tpjbmO696mRlnrrH/5VRtAQK/XQQjrDLJRvefgRDqyT0RIZdZYT5GBbt950owGOWdyU3XMVnvUvZWkedepsBMXyq/M7P4x1+/dvdlkfASQHA/hbOrJYiLWvi5S5/9xCLluFuS/grYvvQ50XgRoDihS86bVAtmFHj1c8GVhYZz40AVVSHlg7ifzMZ9er6IHfIaqQjZ4meJU8RNkZ9tyFMeovIovzNHwwbN6RGhpwJgdHkOEy4nSV7HkGAIrQ4XtjGNIpHPB71SQzGoiFMRXtyBAHqE+vFHLyChWqvNqu3bLPea2Sa9yOaJ8pjltt9Hyff4mTUhJ3kDkVwoNNhfYHMyHQAqYQgdMXHChhRQiOLY0aybu2Y0dFR07OWfutIP5weMy3jSs2hSzDTDz+SxtMiLoraazZj3jomumnnoVvCuLuOVaJMduw+I+Xzj9bh/+zAdcBZT7FyeHNhyAqCAmd6maMCGgJ/c5jvZBP+UQ== maxim@ebi.ac.uk"
  },
  {
    user = "mbc",
    id   = "8652",
    sudo = "no",
    home = "/home/user/mbc",
    groups = [ "mm" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDKmacT73Y4JZ2kcBI8i105wWgDwHFluVPgfEv+1c65sLDWcriRqOR+hKuEIfF+ucgas9Uq9S9B8BJehPwlSFBlxkCUaYZiZD6605YCTYmU7yVJ66i/mo/rDb005teXQs0rNxCSCauOcFFZBPUPfcJ3bHP7hAOKL2szRzq4eHwNeOvwPbPukmAuCNlFpONSJNJPOAsAkH9RKBEv91WV0AhpDapAx+ZSgfwBF5spgDWv9kEgBZ9JGkfRTFahxb4PhDaFoFYhwhKFOsF6fobvQyCsdH/P3T/h0J6FJ6pRE0BP2lJZ3urRekVOd8ktdDXup1waZHiwbV39hTN4AL+89Bnp8J58+XqEfGw/rW+Lneu0Sl4bYA9fFh5cJ7N8afqRJIwVaL6R6zVh9llOt4+/gFb6pLKBtUxStd5alXkNVTWT28dyPwt07/2EdTQpYU5aS/wY5BwEH4M3xVkZoX0NSS8xGKuYt0isRrmOOX2jSp3kGzUZ91dsDsqXmcXlX679elSR8j5lauc/23NrMk4+N1dLPdv2qeJJPXcQe0jZ+H1P4EXuYJsWJOW0lgiw+6yhsF1jMhckp9dHnMik8BIjTFuSYtAXtAv7XizHKiifIZLMGcGjAYTcSXpljMbyNcfYR+JJ4026HydXmz10PrNhFSv1H/8jVZBcVVGhfr6FZ/+a1w== mbc@ebi.ac.uk"
  },
  {
    user = "kates",
    id   = "8592",
    sudo = "no",
    home = "/home/user/kates",
    groups = [ "mm" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwo5UESGqxL3LDjnYzAw4/mD0MiuGvK21TPzK8nYepV9kr5ZfIu5N47xHIgPx3T3YXljiT8+lUKZKWIVIdepp5fYhFyOBXTeD3DIkjlcbZU7hpdm31z7jSL3qATtcN1Uvkpj4Ppsw9rarQe9RzDYIEY3j6yYUFpTznRxpJ++jbFu51HizL1VsKbIKiN2LRCCFH3iI4lGaX7ZZA06jP03T05ulqFfxP8oZQdWaOXVPztBoGdojXFv9oEzXJtsvNxrhd9R+U8UR3T4WUiPr9/CSqM/3PkZHowM3YDRRMM/i5bk0XcZkYAbjid7yji3objiXofG1+KV3fvr0kh8p0mx+B kates@C02TV020HV2T"
  },
  # {
  #   user = "saary",
  #   id   = "7625",
  #   sudo = "no",
  #   home = "/home/user/saary",
  #   groups = [ "mm" ],
  #   key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC64Qt11qqyBc0KgybeJIEd9w9pIkHQ0Jc96412LoxpWCk9o7NZZPeTJ1KjN6uv/5Q9yH1RP+vKEayO/m9xDCFDl53PsAjrvE0xA9lswXUKn6aCcuhnCxcOyjL1r74TE/RlZ9Kfb+H+ZYAIVjrfdWFQ82vP8rT+GsKFXAfTLviFwabA9+1xu7aRqW03c0EQ6F2Rl4RnFDoRysiRH7XFucyusN/CKi3lXAjRXX5vesMFZkBLmlPxTBxEy09mT3BnwsLMtf61vTASngcZNSZCQX/2bTvKtgg5qyo6XiHOs5ysAJlf28LP0XxZ4c8gZgmvHSpdt5PmOlQgc8yr5VI+vQCjsbjyoynrz3iQghmsH5z072qoSW2HNHJeDzGsv+UzoVTKyhXBZz1SRx2NRjUhEy1I06Nnd0Tj+rMC06nyCy9XIDysXkQOglV6eWl501VPq5NTMTHjrD/gXvHXW1m3ZCt9ik1tLPwLbu8+/nUQAok5Gd5VUvoJ0JcWVRB+Z2wlpJob+7TtdMh9c3VAcP6yLO3YxRdbOnkuq+PLJGHWouh8nbZxYsESRXkf7QrHgzwqMTJIqha1ZJLRncCPV4QECFJEme8X68te75bx9zl25EoqfnC7JjZiDaFCmAWo3bvs1m1R0kSe6NCY5FRzZudnmCxeqm8N5azNJQlawUsXC6JOUQ== paul@flatwhite"
  # },
  {
    user = "vkale",
    id   = "8582",
    sudo = "no",
    home = "/home/user/vkale",
    groups = [ "mm" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC2hDHjht3p/TuLyqYehiOwx8aQmNcNcOww4kM/KRu5Qpr2p2XNIallry/DOxJyUwYGoaYld6ac+HD4zUVrUTFoDw8VUEtGHvvHZRONScPks1io8yc+AtA3NgQzogoVIA3N5FALMW3Wp9l1vZzZbv1S7yOej72oGHZpm9rYoGsFY26GODAUwUa9w4PaxDwOewlrwqBQgAtS4sCzJ7h+Te8paDhiuLR7Fvl/Oh5bv/NOWmt/NxOuzwaaio1EqkH2RsG+PDIOCn+0j+EP2IDNk4TWTv2kajbzEtLGGTlLr53ydifzbdHtIqBGAjhsom0abpio3TxpnEpB7COKaw9GlFlD vkale@ebi.ac.uk"
  },
  {
    user = "lornar",
    id   = "3117",
    sudo = "no",
    home = "/home/user/lornar",
    groups = [ "mm" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDGh8bog7J2uk/BZUmAyI3H0F9bMyjrzbAlx6tUq+w11iARfqbcru8Gx/YJYms7ykFXXQianWWgR9g6XtvKNO2YG8K1QFtvwldRQ+KpVz3o9vAP67xObbcsr9UtkELSqfsHwH2Fkt0WwGp9PxhcaFc8ag2gVXLEcn1WG/WSUS4olZqbPH2wrIuL8vZmLCD8xaq4p3NsTKyJUu6huftNFkTdI/04mKUqQcrv3C5gmkI2LqoGEzTzuHCPcZstbnD0REfvBcLYyhEQgQ5WM671vXtDoTrWoATgPteq906C3ZTmBMiD8o0NR8DfeQhwsfHLIAcWe/1RLi/8frQk8dXi/G4R lornar@lornar-ml"
  },
]

lsf_source = "../LSF" # Relative to the directory with the lsf terraform installation script
lsf_version = 10.1
