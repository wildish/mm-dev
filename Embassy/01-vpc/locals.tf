data "terraform_remote_state" "state" {
  backend = "local"
  config = {
    path = "../00-config/terraform.tfstate"
  }
}

locals {
  namespace        = data.terraform_remote_state.state.outputs.namespace
  subnet           = data.terraform_remote_state.state.outputs.subnet
  external_network = data.terraform_remote_state.state.outputs.external_network

  state = data.terraform_remote_state.state.outputs.state

  default_security_group = data.terraform_remote_state.state.outputs.default_security_group
  public_key             = data.terraform_remote_state.state.outputs.public_key
  private_key            = data.terraform_remote_state.state.outputs.private_key

  tenancy_user = data.terraform_remote_state.state.outputs.tenancy_user
}

