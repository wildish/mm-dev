resource "null_resource" "s3_bastion" {

  connection {
    user        = local.user
    private_key = file(local.private_key)
    agent       = false
    host        = local.floating_ip
  }

# triggers = { something = "${uuid()}" }

  provisioner "file" {
    content = <<EOF
export AWS_ACCESS_KEY_ID=${local.s3_access_key}
export AWS_SECRET_ACCESS_KEY=${local.s3_secret_key}
export S3_BUCKET=s3://${local.s3_bucket}
export S3='aws --endpoint-url https://s3.embassy.ebi.ac.uk s3'
EOF
    destination = "/home/${local.user}/bin/s3.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo cp /home/${local.user}/bin/s3.sh /etc/profile.d/s3.sh",
      "sudo /home/${local.user}/bin/s3-cli.sh",
      "sudo cp /home/${local.user}/bin/s3-slurm-example.sh /slurm"
    ]
  }
}

resource "null_resource" "s3_hosts" {
  depends_on = [ null_resource.s3_bastion ]

  connection {
    user        = local.user
    private_key = file(local.private_key)
    agent       = false
    host                = element(local.host_ips, count.index)
    bastion_private_key = file(local.private_key)
    bastion_host        = local.floating_ip
    bastion_user        = local.user
  }

  count = local.host_count

  # triggers = { something = "${uuid()}" }

  provisioner "file" {
    content = <<EOF
export AWS_ACCESS_KEY_ID=${local.s3_access_key}
export AWS_SECRET_ACCESS_KEY=${local.s3_secret_key}
export S3_BUCKET=s3://${local.s3_bucket}
export S3='aws --endpoint-url https://s3.embassy.ebi.ac.uk s3'
EOF
    destination = "/home/${local.user}/bin/s3.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo cp /home/${local.user}/bin/s3.sh /etc/profile.d/s3.sh",
      "sudo /home/${local.user}/bin/s3-cli.sh"
    ]
  }
}
