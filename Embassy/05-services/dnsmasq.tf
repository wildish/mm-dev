resource "null_resource" "dnsmasq_bastion" {

  connection {
    user        = local.user
    private_key = file(local.private_key)
    agent       = false
    host        = local.floating_ip
  }

  # triggers { something = "${uuid()}" }

  provisioner "local-exec" {
    command = "../bin/make-etc-hosts.sh ${local.state} ${local.namespace}"
  }
  provisioner "file" {
    source      = "${local.state}/hosts.dnsmasq"
    destination = "/home/${local.user}/hosts.dnsmasq"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/dnsmasq.sh ${local.bastion_ip} /home/${local.user}/hosts.dnsmasq",
    ]
  }
}

resource "null_resource" "dnsmasq_hosts" {
  depends_on = [ null_resource.dnsmasq_bastion ]

  connection {
    user        = local.user
    private_key = file(local.private_key)
    agent       = false
    host                = element(local.host_ips, count.index)
    bastion_private_key = file(local.private_key)
    bastion_host        = local.floating_ip
    bastion_user        = local.user
  }

  count = local.host_count

  # triggers { something = "${uuid()}" }

  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/dnsmasq.sh ${local.bastion_ip}",
    ]
  }
}
