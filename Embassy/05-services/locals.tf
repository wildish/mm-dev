data "terraform_remote_state" "state" {
  backend = "local"
  config = {
    path = "../00-config/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "../01-vpc/terraform.tfstate"
  }
}

data "terraform_remote_state" "bastion" {
  backend = "local"
  config = {
    path = "../03-bastion/terraform.tfstate"
  }
}

data "terraform_remote_state" "hosts" {
  backend = "local"
  config = {
    path = "../04-hosts/terraform.tfstate"
  }
}

locals {
  state = data.terraform_remote_state.state.outputs.state

  namespace = data.terraform_remote_state.state.outputs.namespace

  floating_ip = data.terraform_remote_state.vpc.outputs.floating_ip
  bastion_ip  = data.terraform_remote_state.bastion.outputs.bastion_ip

  public_key  = data.terraform_remote_state.state.outputs.public_key
  private_key = data.terraform_remote_state.state.outputs.private_key

  user = data.terraform_remote_state.state.outputs.tenancy_user
  host_count = data.terraform_remote_state.state.outputs.host_count
  host_ips   = data.terraform_remote_state.hosts.outputs.host_ips

  s3_access_key = data.terraform_remote_state.state.outputs.s3_access_key
  s3_secret_key = data.terraform_remote_state.state.outputs.s3_secret_key
  s3_bucket     = data.terraform_remote_state.state.outputs.s3_bucket
}
