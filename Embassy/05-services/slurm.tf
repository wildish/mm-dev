# resource "null_resource" "slurm_install_bastion" {
#   depends_on = [ null_resource.dnsmasq_hosts ]
#   connection {
#     user        = local.user
#     private_key = file(local.private_key)
#     agent       = false
#     host        = local.floating_ip
#   }

#   provisioner "remote-exec" {
#     inline = [
#       "sudo /home/${local.user}/bin/slurm-install.sh",
#     ]
#   }
# }

# resource "null_resource" "slurm_install_hosts" {
#   depends_on = [ null_resource.slurm_install_bastion ]

#   connection {
#     user        = local.user
#     private_key = file(local.private_key)
#     agent       = false
#     host                = element(local.host_ips, count.index)
#     bastion_private_key = file(local.private_key)
#     bastion_host        = local.floating_ip
#     bastion_user        = local.user
#   }

#   count = local.host_count
#   provisioner "remote-exec" {
#     inline = [
#       "sleep  ${count.index * 3}", # stagger the starts to not hammer the NFS so hard
#       "sudo /home/${local.user}/bin/slurm-install.sh",
#     ]
#   }
# }

# #----------------------------
# resource "null_resource" "slurm_enable_bastion" {
#   depends_on = [ null_resource.slurm_install_hosts ]
#   connection {
#     user        = local.user
#     private_key = file(local.private_key)
#     agent       = false
#     host        = local.floating_ip
#   }

#   provisioner "remote-exec" {
#     inline = [
#       "sudo /home/${local.user}/bin/slurm-enable.sh ${local.namespace} ${local.host_count}",
#     ]
#   }
# }

# resource "null_resource" "slurm_enable_hosts" {
#   depends_on = [ null_resource.slurm_enable_bastion ]

#   connection {
#     user        = local.user
#     private_key = file(local.private_key)
#     agent       = false
#     host                = element(local.host_ips, count.index)
#     bastion_private_key = file(local.private_key)
#     bastion_host        = local.floating_ip
#     bastion_user        = local.user
#   }

#   # triggers = { something = "${uuid()}" }
#   count = local.host_count
#   provisioner "remote-exec" {
#     inline = [
#       "sudo /home/${local.user}/bin/slurm-enable.sh",
#     ]
#   }
# }

# #----------------------------
# resource "null_resource" "slurm_start_bastion" {
#   depends_on = [ null_resource.slurm_enable_hosts ]
#   connection {
#     user        = local.user
#     private_key = file(local.private_key)
#     agent       = false
#     host        = local.floating_ip
#   }

#   provisioner "remote-exec" {
#     inline = [
#       "sudo /home/${local.user}/bin/slurm-start.sh",
#     ]
#   }
# }

# resource "null_resource" "slurm_start_hosts" {
#   depends_on = [ null_resource.slurm_start_bastion ]

#   connection {
#     user        = local.user
#     private_key = file(local.private_key)
#     agent       = false
#     host                = element(local.host_ips, count.index)
#     bastion_private_key = file(local.private_key)
#     bastion_host        = local.floating_ip
#     bastion_user        = local.user
#   }

#   count = local.host_count
#   provisioner "remote-exec" {
#     inline = [
#       "sudo /home/${local.user}/bin/slurm-start.sh",
#     ]
#   }
# }
