data "openstack_images_image_v2" "bastion_image" {
  name        = "${local.bastion_image}"
  most_recent = true
}

locals {
  bastion_image_id = "${data.openstack_images_image_v2.bastion_image.id}"
}

resource "openstack_compute_instance_v2" "bastion" {

  availability_zone = local.availability_zone
  flavor_name       = local.bastion_flavour
  security_groups   = local.security_groups
  name              = "${local.namespace}-bastion"

  block_device {
    uuid                  = local.bastion_image_id
    boot_index            = 0
    delete_on_termination = true
    destination_type      = "volume"
    source_type           = "image"
    volume_size           = local.bastion_disk_gb
  }

  network {
    name = local.network
  }

  key_pair = local.key_pair

  provisioner "local-exec" {
    command = "../bin/ip-fragment.sh ${local.state} ${openstack_compute_instance_v2.bastion.access_ip_v4} bastion"
  }
}

resource "openstack_compute_floatingip_associate_v2" "floatingip_bastion" {
  floating_ip           = local.floating_ip
  instance_id           = openstack_compute_instance_v2.bastion.id
  fixed_ip              = openstack_compute_instance_v2.bastion.access_ip_v4
  wait_until_associated = "true"
}

resource "openstack_compute_volume_attach_v2" "volume_attach" {
  count = length( local.bastion_volumes )
  instance_id = openstack_compute_instance_v2.bastion.id
  volume_id   = element( local.bastion_volume_ids, count.index )
}

resource "null_resource" "bastion_post_system_config" {

  depends_on = [
    openstack_compute_floatingip_associate_v2.floatingip_bastion
  ]
  connection {
    user        = local.user
    private_key = file(local.private_key)
    agent       = false
    host        = local.floating_ip
  }

  provisioner "file" {
    source      = "../bin"
    destination = "/home/${local.user}"
  }
  provisioner "file" {
    source      = local.state
    destination = "/home/${local.user}/"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod 755 /home/${local.user}/bin/*",
      "sudo /home/${local.user}/bin/format-mount-export.sh /home/${local.user}/state/metadata.bastion_volumes.json",
      "sudo /home/${local.user}/bin/add-groups.sh /home/${local.user}/state/metadata.groups.json",
      "sudo /home/${local.user}/bin/add-users.sh /home/${local.user}/state/metadata.users.json /home/${local.user}/state/user-keys/",
      "sudo /home/${local.user}/bin/nfs-server.sh",
      "sudo /home/${local.user}/bin/mysql-server.sh",
      # "sudo /home/${local.user}/bin/build-slurm.sh"
    ]
  }
}
