data "terraform_remote_state" "state" {
  backend = "local"
  config = {
    path = "../00-config/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "../01-vpc/terraform.tfstate"
  }
}

data "terraform_remote_state" "storage" {
  backend = "local"
  config = {
    path = "../02-storage/terraform.tfstate"
  }
}

locals {
  state = data.terraform_remote_state.state.outputs.state

  namespace = data.terraform_remote_state.state.outputs.namespace
  availability_zone = data.terraform_remote_state.state.outputs.availability_zone

  floating_ip = data.terraform_remote_state.vpc.outputs.floating_ip
  network = data.terraform_remote_state.vpc.outputs.network

  security_groups = data.terraform_remote_state.vpc.outputs.security_groups
  public_key = data.terraform_remote_state.state.outputs.public_key
  private_key = data.terraform_remote_state.state.outputs.private_key
  key_pair = data.terraform_remote_state.vpc.outputs.key_pair

  user = data.terraform_remote_state.state.outputs.tenancy_user

  bastion_image = data.terraform_remote_state.state.outputs.bastion_image
  bastion_disk_gb = data.terraform_remote_state.state.outputs.bastion_disk_gb
  bastion_flavour = data.terraform_remote_state.state.outputs.bastion_flavour

  bastion_volume_ids = data.terraform_remote_state.storage.outputs.bastion_volume_ids
  bastion_volumes = data.terraform_remote_state.storage.outputs.bastion_volumes
}
