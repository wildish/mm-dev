provider "openstack" {
  user_name   = data.terraform_remote_state.state.outputs.cloud_user
  password    = data.terraform_remote_state.state.outputs.password
  tenant_name = data.terraform_remote_state.state.outputs.tenancy
  auth_url    = data.terraform_remote_state.state.outputs.auth_url
  version     = "~> 1.8"
}

