data "openstack_images_image_v2" "host_image" {
  name        = local.host_image
  most_recent = true
}

locals {
  host_image_id = data.openstack_images_image_v2.host_image.id
}

resource "openstack_compute_instance_v2" "host" {

  availability_zone = local.availability_zone
  flavor_name       = local.host_flavour
  security_groups   = local.security_groups
  name              = format("%s-h-%04d", local.namespace, count.index)

  image_id = local.host_image_id

  network {
    name = local.network
  }

  key_pair = local.key_pair

  count = local.host_count
}

resource "null_resource" "host_post_system_config" {

  depends_on = [ openstack_compute_instance_v2.host ]

  count = local.host_count
  connection {
    user                = local.user
    private_key         = file(local.private_key)
    agent               = false
    host                = element(openstack_compute_instance_v2.host.*.access_ip_v4, count.index)
    bastion_private_key = file(local.private_key)
    bastion_host        = local.floating_ip
    bastion_user        = local.user
  }

  provisioner "file" {
    source      = "../bin"
    destination = "/home/${local.user}"
  }
  provisioner "file" {
    source      = local.state
    destination = "/home/${local.user}/"
  }

  provisioner "remote-exec" {
    inline = [
        "chmod 755 /home/${local.user}/bin/*",
        "sudo /home/${local.user}/bin/add-groups.sh /home/${local.user}/state/metadata.groups.json",
        "sudo /home/${local.user}/bin/add-users.sh /home/${local.user}/state/metadata.users.json /home/${local.user}/state/user-keys/",

        "sudo /home/${local.user}/bin/set-bastion-host-entry.sh ${local.bastion_ip} ${local.namespace}",
        "sudo /home/${local.user}/bin/mount-nfs-disks.sh /home/${local.user}/state/metadata.bastion_volumes.json",
        "sudo setsebool -P use_nfs_home_dirs 1", # Tell SELINUX to allow SSH to work with NFS home directories
    ]
  }

  provisioner "local-exec" {
    command = "../bin/ip-fragment.sh ${local.state} ${element(openstack_compute_instance_v2.host.*.access_ip_v4, count.index)}  ${element(openstack_compute_instance_v2.host.*.name, count.index)}"
  }
}

resource "null_resource" "local_ssh_config" {

  depends_on = [ null_resource.host_post_system_config ]

  # triggers { something = "${uuid()}" }

  provisioner "local-exec" {
    command = "../bin/complete-ssh-config.sh ${local.state} ${local.namespace}"
  }
}
