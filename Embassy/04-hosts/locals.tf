data "terraform_remote_state" "state" {
  backend = "local"
  config = {
    path = "../00-config/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "../01-vpc/terraform.tfstate"
  }
}

data "terraform_remote_state" "storage" {
  backend = "local"
  config = {
    path = "../02-storage/terraform.tfstate"
  }
}

data "terraform_remote_state" "bastion" {
  backend = "local"
  config = {
    path = "../03-bastion/terraform.tfstate"
  }
}

locals {
  state = data.terraform_remote_state.state.outputs.state

  namespace = data.terraform_remote_state.state.outputs.namespace
  availability_zone = data.terraform_remote_state.state.outputs.availability_zone

  bastion_ip = data.terraform_remote_state.bastion.outputs.bastion_ip
  floating_ip = data.terraform_remote_state.vpc.outputs.floating_ip
  network = data.terraform_remote_state.vpc.outputs.network

  security_groups = data.terraform_remote_state.vpc.outputs.security_groups
  public_key = data.terraform_remote_state.state.outputs.public_key
  private_key = data.terraform_remote_state.state.outputs.private_key
  key_pair = data.terraform_remote_state.vpc.outputs.key_pair

  user = data.terraform_remote_state.state.outputs.tenancy_user

  host_image = data.terraform_remote_state.state.outputs.host_image
  host_disk_gb = data.terraform_remote_state.state.outputs.host_disk_gb
  host_flavour = data.terraform_remote_state.state.outputs.host_flavour
  host_count = data.terraform_remote_state.state.outputs.host_count

  storage = data.terraform_remote_state.storage.outputs.bastion_volumes
}
