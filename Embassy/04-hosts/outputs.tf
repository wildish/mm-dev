output "host_ips" {
  description = "ip addresses of the hosts"
  value       = openstack_compute_instance_v2.host.*.access_ip_v4
}
